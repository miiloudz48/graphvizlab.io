---
name: Image format for the Web
params:
- webp
---
Produces output in the image format for the Web (WEBP) format, optimized for
web devices such as tablets.
See Wikipedia's [WebP](http://en.wikipedia.org/wiki/Webp)
or Google's [webp](http://code.google.com/speed/webp/) pages.
