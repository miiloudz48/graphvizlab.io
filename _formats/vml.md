---
name: Vector Markup Language (VML)
params:
- vml
- vmlz
---
Produces [VML](http://www.w3.org/TR/NOTE-VML) output,
the latter in compressed format.

See [Note](#ID).
